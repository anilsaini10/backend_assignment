const { MongoClient, ServerApiVersion } = require('mongodb');




const createCustomer = async (req, res) => {

    const uri = "mongodb+srv://anilsaini:Asdf1234@cluster0.c62fy.mongodb.net/?retryWrites=true&w=majority";
    const client = new MongoClient(uri);

    const db = client.db("All_APIs");

    const collection = db.collection('customers');

    await collection.insert({

        "name": req.body.name,
        "email": req.body.email,
        "number": req.body.number,
        "city": req.body.city,

    });



    return res.send({ "Success": "done" });
}


const purchaseOrder = async (req, res) => {


    // If price is greater than mrp then send warning
    if(req.body.price>req.body.mrp){

        return res.send({ "Warning": "Price can't be less than MRP" });

    }
    
    const uri = "mongodb+srv://anilsaini:Asdf1234@cluster0.c62fy.mongodb.net/?retryWrites=true&w=majority";
    
    const client = new MongoClient(uri);

    const db = client.db("All_APIs");

    const collection = db.collection('orders');

    await collection.insert({

        "product_name": req.body.product_name,
        "quantity": req.body.quantity,
        "price": req.body.price,
        "mrp": req.body.mrp,
        "customer_Id": req.body.customer_Id

    });



    return res.send({ "Success": "done" });
}


const shipingDetails = async (req, res) => {


    console.log(req.body);
    const uri = "mongodb+srv://anilsaini:Asdf1234@cluster0.c62fy.mongodb.net/?retryWrites=true&w=majority";
    const client = new MongoClient(uri);

    const db = client.db("All_APIs");

    const collection = db.collection('shippingDetails');

    await collection.insert({

        "address": req.body.address,
        "city": req.body.city,
        "pincode": req.body.pincode,
        "purchase_Id": req.body.purchase_Id,
        "customer_Id": req.body.customer_Id

    });



    return res.send({ "Success": "done" });

}





module.exports = {

    customer: createCustomer,
    order: purchaseOrder,
    shipping: shipingDetails,


};
