const { MongoClient, ServerApiVersion } = require('mongodb');


const uri = "mongodb+srv://anilsaini:Asdf1234@cluster0.c62fy.mongodb.net/?retryWrites=true&w=majority";
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true, serverApi: ServerApiVersion.v1 });

const cityFiltter = async (req, res) => {


    // take query
    const city_name = req.query;

    const uri = "mongodb+srv://anilsaini:Asdf1234@cluster0.c62fy.mongodb.net/?retryWrites=true&w=majority";
    const client = new MongoClient(uri);

    const db = client.db("All_APIs");

    var data;

    const collection = db.collection('shippingDetails').find({ city: city_name }).toArray((err, result) => {

        
        data = result
    });



    return res.send({ "Success": data });
}


const allCustomers_Orders = async (req, res) => {



    const uri = "mongodb+srv://anilsaini:Asdf1234@cluster0.c62fy.mongodb.net/?retryWrites=true&w=majority";
    const client = new MongoClient(uri);

    const db = client.db("All_APIs");

    // const collection = db.collection('orders').find({}).toArray((err, result) => { console.log(result) });

    var all_orders;
    var all_shipping_details;

    var final_data = [];

    const orders = db.collection('orders').find({}).toArray((err, result) => {
        all_orders = result;

        for (var order in all_orders) {

            var current_data = {};
            var current_list = [];

            const shipping = db.collection('shippingDetails').find({ customer_Id: all_orders[order]["_id"] }).toArray((err, result) => {

                current_list.push(result);
                current_data["customer_Id"] = all_orders[order]["_id"];
                current_data["purchaseOrder"] = current_list;
            })

            final_data.push(current_data);

        }


    });

    return res.send({ "data": final_data });
}


const curstomer_with_purchase_and_shipment_details = async (req, res) => {



    const uri = "mongodb+srv://anilsaini:Asdf1234@cluster0.c62fy.mongodb.net/?retryWrites=true&w=majority";
    const client = new MongoClient(uri);

    const db = client.db("All_APIs");

    var all_orders;
    var all_shipping_details;

    var all_combined_data;

    const orders = db.collection('orders').find({}).toArray((err, result) => {
        all_orders = result;

    });

    const shipping = db.collection('shippingDetails').find({}).toArray((err, result) => {

        all_shipping_details = result;

        var final_data = [];
        var data = {};

        for (var order in all_orders) {


            cus_id = all_orders[order]._id;

            for (var shipping in all_shipping_details) {

                if (all_shipping_details[shipping]["customer_Id"] == cus_id) {

                    data["customer_Id"] = cus_id;
                    data["purchaseOrder"] = {
                        "purchaseOrderId": all_shipping_details[shipping]["purchase_Id"],
                        "productName": all_orders[order]["product_name"],
                        "quantity": all_orders[order]["quantity"],
                        "shippingDetail": [
                            {
                                "address": all_shipping_details[shipping]["address"],
                                "city": all_shipping_details[shipping]["city"],
                                "pincode": all_shipping_details[shipping]["pincode"],

                            }
                        ]
                    }

                }
            }

            final_data.push(data);

        }

    });


    return res.send({ "data": final_data });
}


module.exports = {

    filter_city: cityFiltter,
    all_orders: allCustomers_Orders,
    purchase_and_shipment_details: curstomer_with_purchase_and_shipment_details,

};
