const express = require('express');

const route = express.Router();


const path = require("../controllers/create.js");

const read_path = require("../controllers/read.js");

route.post("/", path.customer);
route.post("/order", path.order);
route.post("/shipping_details", path.shipping);
route.get("/allfilttercity",read_path.filter_city);
route.get("/all_orders",read_path.all_orders);
route.get("/purchase_and_shipment_details", read_path.purchase_and_shipment_details);


module.exports = route;